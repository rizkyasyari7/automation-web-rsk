<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Login</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>17f67252-8757-46c0-874a-a77834693511</testSuiteGuid>
   <testCaseLink>
      <guid>76cac4cb-cf8d-437e-a863-7af5b349a7e5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Login_1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>115c1e3e-691c-4c65-9b38-535ecf9135e5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Login_2</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6a1b7e77-75e9-4b37-ba59-76618714cabc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Login_3</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>59e89cea-4097-407c-b8bd-f1b060a26e02</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Login_4</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>42efac49-f497-4243-9e58-27703fa264f9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Login_5</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d2770310-e115-4c35-81e4-ac26090ef961</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Login_6</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b24ba1ef-7935-4a14-a326-50313883ccb1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Login_7</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>90334536-705b-459c-b8f2-8634a94765ab</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Login_8</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e6e3d03a-de3d-4ea2-b4b7-01604ee285ea</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Login_9</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>516c174e-7d47-43c9-b778-a050fe17f79f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/Login_10</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
