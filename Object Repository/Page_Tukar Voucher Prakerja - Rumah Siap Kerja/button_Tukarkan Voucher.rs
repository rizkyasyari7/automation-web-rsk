<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Tukarkan Voucher</name>
   <tag></tag>
   <elementGuidId>f53833fb-5b05-4269-8a39-8499d706ed32</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/div/div[2]/div/div[7]/div/button</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.btn-pembayaran > button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>acc8649a-43b5-4058-936d-67ad01e4461d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
          Tukarkan Voucher
        </value>
      <webElementGuid>bad39c83-764b-4fbd-a96d-c0ce88037ba3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[1]/div[@class=&quot;header-body-footer-wrapper&quot;]/div[@class=&quot;redemption body&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;voucher&quot;]/div[@class=&quot;btn-pembayaran&quot;]/button[1]</value>
      <webElementGuid>7f87a9ba-ea56-4c43-86ee-6faab3fdbd51</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div/div[2]/div/div[7]/div/button</value>
      <webElementGuid>f694a23e-585e-4930-8d22-97b218b351ba</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=': 823869149'])[1]/following::button[1]</value>
      <webElementGuid>6b13e390-9afd-4204-9dc6-92174202cd0f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Nomor Handphone'])[1]/following::button[1]</value>
      <webElementGuid>88bd1aa2-53b9-42f3-b4fd-3b898feef350</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='LOKASI KAMI'])[1]/preceding::button[1]</value>
      <webElementGuid>e11ac058-95fd-4e2b-b816-43168b16075a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Tukarkan Voucher']/parent::*</value>
      <webElementGuid>763079dc-9e5d-4def-919a-a2ef2e8b0b86</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[7]/div/button</value>
      <webElementGuid>b335f170-5f0c-420e-87ca-91ceba50aa7e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = '
          Tukarkan Voucher
        ' or . = '
          Tukarkan Voucher
        ')]</value>
      <webElementGuid>f5a99386-a497-4ebf-9520-c20f1d8946b2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
