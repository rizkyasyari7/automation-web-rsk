<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Belajar Bisnis untuk Menjadi Pengusaha Online</name>
   <tag></tag>
   <elementGuidId>e291c741-c203-4c24-9422-8f961ad62e00</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/div/div[2]/div[2]/div/div/div/div/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.training-title > span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>ea54d315-90a2-4b79-b272-f138701e90dd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Belajar Bisnis untuk Menjadi Pengusaha Online</value>
      <webElementGuid>67e465ad-4164-4bd6-804e-6b563c3aafdd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[1]/div[@class=&quot;header-body-footer-wrapper&quot;]/div[@class=&quot;training body&quot;]/div[@class=&quot;section-1-background&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;training-short-detail-section&quot;]/div[@class=&quot;detail-section-right&quot;]/div[@class=&quot;training-title&quot;]/span[1]</value>
      <webElementGuid>81eb5702-2611-404d-9b2f-02962cf4cb19</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div/div[2]/div[2]/div/div/div/div/span</value>
      <webElementGuid>2412ddb3-2dbd-477a-af9a-a2d0b336507e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Masuk Kelas'])[1]/following::span[1]</value>
      <webElementGuid>370f4cca-461a-448d-ab61-78812357fbc8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jadwal Kelas:'])[1]/following::span[1]</value>
      <webElementGuid>94105a1d-881a-472d-8abe-e742317328a2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kartu Prakerja'])[2]/preceding::span[1]</value>
      <webElementGuid>6ad02a67-e347-4fa0-804c-083f44c48e1d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Bisnis &amp; Keuangan'])[1]/preceding::span[1]</value>
      <webElementGuid>464659d9-a6d1-4843-8b84-79381ae1fbc1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Belajar Bisnis untuk Menjadi Pengusaha Online']/parent::*</value>
      <webElementGuid>8a264ce4-c827-43d9-8b06-2c492ef607e9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div/div/div/div/span</value>
      <webElementGuid>456c8463-d89a-47c1-a376-499cea525710</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Belajar Bisnis untuk Menjadi Pengusaha Online' or . = 'Belajar Bisnis untuk Menjadi Pengusaha Online')]</value>
      <webElementGuid>4478109d-4dc3-4a40-971a-4507e9d1c73e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
