<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Detail Kelas</name>
   <tag></tag>
   <elementGuidId>67312058-4b52-4392-8027-f1d7ede2cf98</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/div/div[2]/div[2]/div[2]/div/div/div/div/div[2]/div/button</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>d8b926bc-c409-4005-a080-916312fe5dd5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        Detail Kelas
      </value>
      <webElementGuid>eb10f55b-ca4b-44a4-961b-00fee47ac740</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[1]/div[@class=&quot;header-body-footer-wrapper&quot;]/div[@class=&quot;body&quot;]/div[@class=&quot;kelas-saya&quot;]/div[@class=&quot;my-list&quot;]/div[@class=&quot;my-list&quot;]/div[@class=&quot;content&quot;]/div[@class=&quot;item&quot;]/div[@class=&quot;card spacing&quot;]/div[@class=&quot;training-side-menu&quot;]/div[@class=&quot;wrapper child-2&quot;]/button[1]</value>
      <webElementGuid>967715a1-094f-4309-a2cb-6bfeac25364d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div/div[2]/div[2]/div[2]/div/div/div/div/div[2]/div/button</value>
      <webElementGuid>6d3fafa9-bab6-4490-9efd-52cdb086efa6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kelas video'])[1]/following::button[1]</value>
      <webElementGuid>264b04bd-f777-4fd2-9336-fd7e479bab07</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Belajar Bisnis untuk Menjadi Pengusaha Online'])[1]/following::button[1]</value>
      <webElementGuid>a8b18ca8-628c-44b2-bb27-5d8aff683384</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Masuk Kelas'])[1]/preceding::button[1]</value>
      <webElementGuid>606dfb47-28db-49ba-9d69-5fb8cb7dce20</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Detail Kelas']/parent::*</value>
      <webElementGuid>6e650635-78f6-40b8-b124-41b29a0668ad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button</value>
      <webElementGuid>a1dab86c-b021-456d-b6e9-5b35775760f9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = '
        Detail Kelas
      ' or . = '
        Detail Kelas
      ')]</value>
      <webElementGuid>0dd12d47-6a0c-4ef3-9c6b-89c8f085795e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
