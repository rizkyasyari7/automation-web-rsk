<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Kelas Aktif</name>
   <tag></tag>
   <elementGuidId>943d89c6-2a7e-4602-a29a-95c7389ff6b9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/div/div[2]/div[2]/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.training.spacing > div.title</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>ed946232-0674-4486-abdc-f4cb1cd94e8e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>title</value>
      <webElementGuid>9f865150-fad8-48d7-b04b-ad0eae96e28f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        Kelas Aktif
      </value>
      <webElementGuid>f577d7cc-5f93-4c98-bd2c-8191834cc37c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[1]/div[@class=&quot;header-body-footer-wrapper&quot;]/div[@class=&quot;body&quot;]/div[@class=&quot;kelas-saya&quot;]/div[@class=&quot;training spacing&quot;]/div[@class=&quot;title&quot;]</value>
      <webElementGuid>4a3e5b1f-4afc-4010-9864-09caf92c047d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div/div[2]/div[2]/div/div</value>
      <webElementGuid>5c0aa289-5d0b-4634-a60a-05953c377aad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Transaksi'])[1]/following::div[3]</value>
      <webElementGuid>a5cd3b6d-851f-43a9-a3de-179c5be43a2e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kelas'])[2]/following::div[4]</value>
      <webElementGuid>d58cea27-18c8-4ecb-bd5f-de60dac992ab</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Belajar Bisnis untuk Menjadi Pengusaha Online'])[1]/preceding::div[2]</value>
      <webElementGuid>465bf777-8405-4cfa-a021-1105122598e9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kelas video'])[1]/preceding::div[3]</value>
      <webElementGuid>85226107-9e75-43dd-9838-bc10a3db8bd5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Kelas Aktif']/parent::*</value>
      <webElementGuid>e2ac9593-4754-47cc-bf95-360048bc91e5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div/div</value>
      <webElementGuid>b636220b-fd14-451b-b6b1-c7ea305217a4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
        Kelas Aktif
      ' or . = '
        Kelas Aktif
      ')]</value>
      <webElementGuid>5f48fd64-82a0-4f46-bbed-9f0ae8697f86</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
