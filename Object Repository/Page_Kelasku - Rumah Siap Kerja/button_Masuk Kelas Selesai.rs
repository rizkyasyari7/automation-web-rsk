<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Masuk Kelas Selesai</name>
   <tag></tag>
   <elementGuidId>ea7775f6-6088-4d78-94d1-19ac3cc34db8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/div/div[2]/div[2]/div[3]/div/div/div/div/div[2]/div/button[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>a57a5f94-061d-4bb7-9f0f-aa40eebf0071</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        Masuk Kelas
      </value>
      <webElementGuid>23cd8d53-13f4-41be-94c4-7bc7dc8e48ef</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[1]/div[@class=&quot;header-body-footer-wrapper&quot;]/div[@class=&quot;body&quot;]/div[@class=&quot;kelas-saya&quot;]/div[@class=&quot;my-list&quot;]/div[@class=&quot;my-list&quot;]/div[@class=&quot;content&quot;]/div[@class=&quot;item&quot;]/div[@class=&quot;card&quot;]/div[@class=&quot;training-side-menu&quot;]/div[@class=&quot;wrapper child-2&quot;]/button[2]</value>
      <webElementGuid>08c8063c-0e8c-48fe-b7a8-ff56885a9aa8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div/div[2]/div[2]/div[3]/div/div/div/div/div[2]/div/button[2]</value>
      <webElementGuid>d6d09528-2482-467e-99b8-96b129336072</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Beri Ulasan'])[1]/following::button[1]</value>
      <webElementGuid>8e7ac863-ee70-4bf7-9805-07e0ebfc0f6f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kelas video'])[1]/following::button[2]</value>
      <webElementGuid>1bb07fef-2e05-4abc-bd31-75249af61893</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='LOKASI KAMI'])[1]/preceding::button[1]</value>
      <webElementGuid>cf789cfb-e197-4c29-ae75-6418908766f5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Masuk Kelas']/parent::*</value>
      <webElementGuid>bbefb257-ac2c-4137-a632-400da645e0e8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button[2]</value>
      <webElementGuid>0b9ea44b-9b9f-47bf-95e2-8d76e2ca1fad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = '
        Masuk Kelas
      ' or . = '
        Masuk Kelas
      ')]</value>
      <webElementGuid>a05c9052-51cb-4c2f-934c-ee2745aafc1f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
