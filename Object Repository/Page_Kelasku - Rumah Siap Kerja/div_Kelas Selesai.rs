<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Kelas Selesai</name>
   <tag></tag>
   <elementGuidId>2ca780ae-2780-44f0-b1ba-b9daca203b86</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/div/div[2]/div[2]/div[2]/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.training.spacing > div.title</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>9d7d4f92-9a7a-4a87-a141-fa314a4c4fdd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>title</value>
      <webElementGuid>4a1ee426-419e-4a09-a092-97283a522431</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        Kelas Selesai
      </value>
      <webElementGuid>24ae911f-9822-4a0c-8e20-25f8b8ee7db5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[1]/div[@class=&quot;header-body-footer-wrapper&quot;]/div[@class=&quot;body&quot;]/div[@class=&quot;kelas-saya&quot;]/div[@class=&quot;training spacing&quot;]/div[@class=&quot;title&quot;]</value>
      <webElementGuid>c20a1cc6-b3b1-4078-b88d-5843ad7f8b68</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div/div[2]/div[2]/div[2]/div</value>
      <webElementGuid>f2de19a6-c311-4819-92c1-7fd98b571107</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kelas Aktif'])[1]/following::div[2]</value>
      <webElementGuid>1f8460dc-ebe0-4f9c-bd20-a1462d401149</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Transaksi'])[1]/following::div[5]</value>
      <webElementGuid>4d7005c4-27d9-403f-8736-a261cc005f1d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Menjadi Pemandu Wisata (Tour Guide) dengan Menguasai Tekniknya'])[1]/preceding::div[2]</value>
      <webElementGuid>b3d494f9-ce4e-499a-a0a9-22fb04884f6f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kelas video'])[1]/preceding::div[3]</value>
      <webElementGuid>88001150-10e5-4a21-90ce-d08808baa6b7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Kelas Selesai']/parent::*</value>
      <webElementGuid>0210c8b5-aa11-48cb-98d3-20d474fd113a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div[2]/div</value>
      <webElementGuid>4b1da2a9-8fc8-4456-ad66-1f90699e8115</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
        Kelas Selesai
      ' or . = '
        Kelas Selesai
      ')]</value>
      <webElementGuid>868503da-e0ff-4802-a016-c4c7e45c8240</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
