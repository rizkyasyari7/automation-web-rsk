<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Detail Kelas Tukar Voucher</name>
   <tag></tag>
   <elementGuidId>ac5444ec-e7bb-4019-95b8-0b14cf3ea070</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/div/div[2]/div[2]/div[2]/div/div/div[2]/div/div[2]/div/button</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>7f85f916-6372-453c-a55a-d34fe9008bea</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        Detail Kelas
      </value>
      <webElementGuid>f671caa8-3410-4d54-aad6-b7040df6acfc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[1]/div[@class=&quot;header-body-footer-wrapper&quot;]/div[@class=&quot;body&quot;]/div[@class=&quot;kelas-saya&quot;]/div[@class=&quot;my-list&quot;]/div[@class=&quot;my-list&quot;]/div[@class=&quot;content&quot;]/div[@class=&quot;item&quot;]/div[@class=&quot;card&quot;]/div[@class=&quot;training-side-menu&quot;]/div[@class=&quot;wrapper child-2&quot;]/button[1]</value>
      <webElementGuid>52362cce-d81b-4f3a-80cf-a74840a47318</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div/div[2]/div[2]/div[2]/div/div/div[2]/div/div[2]/div/button</value>
      <webElementGuid>fd398c2b-afdb-4f86-80d6-6b2543df3e71</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kelas video'])[2]/following::button[1]</value>
      <webElementGuid>c96ba644-b4a3-4479-995a-4db2eed1b355</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Masuk Kelas'])[2]/preceding::button[1]</value>
      <webElementGuid>a143a8b3-96c1-4ece-89ec-87f64c711d53</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Praktek Langsung Menanam Hidroponik'])[1]/preceding::button[2]</value>
      <webElementGuid>39e40194-ea3d-4347-8f9b-f9d89c651e46</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[2]/div/button</value>
      <webElementGuid>78dc5b53-274c-4fba-93d6-7a13cf12736f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = '
        Detail Kelas
      ' or . = '
        Detail Kelas
      ')]</value>
      <webElementGuid>2257dadc-7e4d-4967-8ffa-bbd7febe2420</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
