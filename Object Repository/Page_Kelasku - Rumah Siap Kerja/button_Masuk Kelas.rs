<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Masuk Kelas</name>
   <tag></tag>
   <elementGuidId>f494ebaf-5d19-47d0-8e6a-ca2de02b7d4d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/div/div[2]/div[2]/div[2]/div/div/div/div/div[2]/div/button[2]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>13c389cb-a304-47f7-b77b-10bc2025598a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        Masuk Kelas
      </value>
      <webElementGuid>1e39db17-67f3-4c81-8c76-9ba0d16f5216</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[1]/div[@class=&quot;header-body-footer-wrapper&quot;]/div[@class=&quot;body&quot;]/div[@class=&quot;kelas-saya&quot;]/div[@class=&quot;my-list&quot;]/div[@class=&quot;my-list&quot;]/div[@class=&quot;content&quot;]/div[@class=&quot;item&quot;]/div[@class=&quot;card spacing&quot;]/div[@class=&quot;training-side-menu&quot;]/div[@class=&quot;wrapper child-2&quot;]/button[2]</value>
      <webElementGuid>dc196775-43fa-4d0c-b27b-75bd9c74b49a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div/div[2]/div[2]/div[2]/div/div/div/div/div[2]/div/button[2]</value>
      <webElementGuid>b1d03b42-27bd-4bac-8fb8-ca82d2605492</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Detail Kelas'])[1]/following::button[1]</value>
      <webElementGuid>940edd8b-6cf7-4bd7-bc71-0163d6919cac</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kelas video'])[1]/following::button[2]</value>
      <webElementGuid>cca8b7f3-5309-412c-9b55-8d0487e2cf33</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lengkapi disini.'])[1]/preceding::button[1]</value>
      <webElementGuid>96e852c4-092c-44ce-b818-aae47300b2be</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Masuk Kelas']/parent::*</value>
      <webElementGuid>689282bb-fac5-4589-a5f4-f9442a5c411d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button[2]</value>
      <webElementGuid>5d448b45-578e-4eda-aa95-4b4bfe7498fd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = '
        Masuk Kelas
      ' or . = '
        Masuk Kelas
      ')]</value>
      <webElementGuid>198cf119-5918-482e-9fc2-59000b593d45</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
