<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Beri Ulasan Kelas</name>
   <tag></tag>
   <elementGuidId>d38461ab-6442-4b6b-af13-df722f944e7b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/div/div[2]/div[2]/div[3]/span/div[2]/div/div/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.title-header</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>300583fd-2a6a-4205-9cf4-046f0198506d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>title-header</value>
      <webElementGuid>1e3a15b1-336a-4bdb-9067-85e24c484226</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
            Beri Ulasan Kelas
          </value>
      <webElementGuid>e8491160-8e47-472e-85c1-f9fea3990b24</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[1]/div[@class=&quot;header-body-footer-wrapper&quot;]/div[@class=&quot;body&quot;]/div[@class=&quot;kelas-saya&quot;]/div[@class=&quot;my-list&quot;]/span[1]/div[@class=&quot;modal&quot;]/div[@class=&quot;modal-dialog&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-header&quot;]/div[@class=&quot;title-header&quot;]</value>
      <webElementGuid>dbb7615a-4c13-4e83-8751-6f1e048ee607</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div/div[2]/div[2]/div[3]/span/div[2]/div/div/div/div</value>
      <webElementGuid>6a12fd25-36b8-4995-ae77-987eb3b3c8d0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Masuk Kelas'])[1]/following::div[6]</value>
      <webElementGuid>079a2b76-3f7a-4ac4-bdd6-47659e9bcb1a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Beri Ulasan'])[1]/following::div[6]</value>
      <webElementGuid>b7bc0ec7-989b-4a6a-a72e-86378d6b416e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Menjadi Pemandu Wisata (Tour Guide) dengan Menguasai Tekniknya'])[2]/preceding::div[1]</value>
      <webElementGuid>6439cdc2-5dbd-4509-95a6-f6e09cb9a0e9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pelatih: Chomsyah Putra'])[1]/preceding::div[2]</value>
      <webElementGuid>fd06f8c0-f102-4406-9fd2-f15024d1cd0c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Beri Ulasan Kelas']/parent::*</value>
      <webElementGuid>0c0806f0-7e96-43d1-b851-7d836074ecef</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span/div[2]/div/div/div/div</value>
      <webElementGuid>d392aa24-7b05-4912-b9ed-c9d3c6459e61</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
            Beri Ulasan Kelas
          ' or . = '
            Beri Ulasan Kelas
          ')]</value>
      <webElementGuid>294c86aa-0d5b-40b6-82d3-f00c9309f31a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
