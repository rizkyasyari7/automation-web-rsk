<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Beri Ulasan</name>
   <tag></tag>
   <elementGuidId>ff797729-b895-4437-a2a8-9e27bbb32e56</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/div/div[2]/div[2]/div[3]/div/div/div/div/div[2]/div/button</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>button</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>a27cda58-34c2-4d7b-bcac-9cc36191f99b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        Beri Ulasan
      </value>
      <webElementGuid>09fb66a5-c302-4292-8445-8dc327650b4b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[1]/div[@class=&quot;header-body-footer-wrapper&quot;]/div[@class=&quot;body&quot;]/div[@class=&quot;kelas-saya&quot;]/div[@class=&quot;my-list&quot;]/div[@class=&quot;my-list&quot;]/div[@class=&quot;content&quot;]/div[@class=&quot;item&quot;]/div[@class=&quot;card&quot;]/div[@class=&quot;training-side-menu&quot;]/div[@class=&quot;wrapper child-2&quot;]/button[1]</value>
      <webElementGuid>7acfa7fb-d802-475f-8013-e2ca3c7fbbe2</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div/div[2]/div[2]/div[3]/div/div/div/div/div[2]/div/button</value>
      <webElementGuid>a91e42b2-1e2b-4c12-80b9-6cadcc874e1b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kelas video'])[1]/following::button[1]</value>
      <webElementGuid>e6761609-0423-4e3c-a961-34736bb5cd70</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Menjadi Pemandu Wisata (Tour Guide) dengan Menguasai Tekniknya'])[1]/following::button[1]</value>
      <webElementGuid>c50d46eb-f548-4a55-b3dc-26a43be8d348</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Masuk Kelas'])[1]/preceding::button[1]</value>
      <webElementGuid>ba5219e1-4a77-47b2-999b-e30ae16b1ec2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Beri Ulasan']/parent::*</value>
      <webElementGuid>bd938451-c555-4ff8-8b9a-280f6619c535</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button</value>
      <webElementGuid>225072b5-a8ec-4d24-96e2-fb1b77258212</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[(text() = '
        Beri Ulasan
      ' or . = '
        Beri Ulasan
      ')]</value>
      <webElementGuid>4b8e536d-11f7-4592-bd35-73b3b034a495</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
