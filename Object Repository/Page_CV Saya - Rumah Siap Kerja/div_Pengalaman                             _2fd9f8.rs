<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Pengalaman                             _2fd9f8</name>
   <tag></tag>
   <elementGuidId>aaa11b90-417d-443b-a78f-10e5df86eb98</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.modal</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/div/div[2]/div[3]/span/div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>modal</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
          Pengalaman
           
                      Pekerjaan
                      *   
                      Perusahaan
                      *   
                      Tanggal Mulai
                      *      
                      Periode Waktu
                           
                      Tanggal Berakhir
                      *      
                      Periode Waktu
                            Saat ini bekerja disini 
                      Spesialisasi
                      *      
                      Negara
                      *      
                      Provinsi
                      *      
                      Industri
                      *      
                      Tingkat Jabatan
                         
                      Gaji Bulanan (Rp)
                      
                      Deskripsi Pekerjaan
                      *    Batalkan Simpan</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[1]/div[@class=&quot;header-body-footer-wrapper&quot;]/div[@class=&quot;cv-builder body&quot;]/div[@class=&quot;content&quot;]/span[1]/div[@class=&quot;modal&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div/div[2]/div[3]/span/div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='+ Tambah Pengalaman kerja'])[1]/following::div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pengalaman kerja belum dilengkapi'])[1]/following::div[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span/div[2]</value>
   </webElementXpaths>
</WebElementEntity>
