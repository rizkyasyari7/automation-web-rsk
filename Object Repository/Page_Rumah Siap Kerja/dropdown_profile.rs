<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>dropdown_profile</name>
   <tag></tag>
   <elementGuidId>84f16be2-8ec9-4699-9531-744519889ed7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/div/div/div/div/div[2]/div[2]/div[3]/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.name-and-profile-image</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>name-and-profile-image</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Muhammad
                 Muhammad Rizky Asyari rizky.ashari@rumahsiapkerja.com Profilku Kelasku Sertifikat Kelas 
                    Ganti Kata Sandi
                   Keluar</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[1]/div[@class=&quot;header-body-footer-wrapper&quot;]/div[@class=&quot;header&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;desktop-navbar&quot;]/div[@class=&quot;group-2-and-3-wrapper&quot;]/div[@class=&quot;group-3&quot;]/div[@class=&quot;usermenu&quot;]/div[@class=&quot;name-and-profile-image&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div/div/div/div/div[2]/div[2]/div[3]/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lihat semua'])[1]/following::div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kelas Selesai'])[2]/following::div[4]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[3]/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = 'Muhammad
                 Muhammad Rizky Asyari rizky.ashari@rumahsiapkerja.com Profilku Kelasku Sertifikat Kelas 
                    Ganti Kata Sandi
                   Keluar' or . = 'Muhammad
                 Muhammad Rizky Asyari rizky.ashari@rumahsiapkerja.com Profilku Kelasku Sertifikat Kelas 
                    Ganti Kata Sandi
                   Keluar')]</value>
   </webElementXpaths>
</WebElementEntity>
