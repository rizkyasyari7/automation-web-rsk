<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Belajar Bisnis untuk Menjadi Pengusaha Online</name>
   <tag></tag>
   <elementGuidId>184c8bc8-e268-49d0-b334-7109f0355a44</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Lewati ke konten utama'])[1]/following::span[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.ml-5</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>ad864c9a-23f2-486b-8539-d067fb303989</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ml-5</value>
      <webElementGuid>3d6cf6ca-3811-4c65-96e4-43ddbb89d879</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Belajar Bisnis untuk Menjadi Pengusaha Online</value>
      <webElementGuid>ca5f466f-5733-46fd-98ba-f9fef4b28b16</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;ltr view-in-course view-courseware courseware  lang_id&quot;]/div[@class=&quot;window-wrap&quot;]/div[1]/div[@class=&quot;rsk-container&quot;]/nav[@class=&quot;rsk-navbar&quot;]/div[@class=&quot;rsk-navbar-brand&quot;]/a[@class=&quot;rsk-navbar-item&quot;]/span[@class=&quot;ml-5&quot;]</value>
      <webElementGuid>bf39b165-7633-4fdb-8bfa-37bcf6a9ef61</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lewati ke konten utama'])[1]/following::span[1]</value>
      <webElementGuid>16bd044a-82b2-412f-9209-123fda435638</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kursus'])[1]/preceding::span[1]</value>
      <webElementGuid>c3452099-51d2-4c13-a0cb-8e279bab94cd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Diskusi'])[1]/preceding::span[1]</value>
      <webElementGuid>7c78a097-145e-480b-a6f1-5c8111552640</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Belajar Bisnis untuk Menjadi Pengusaha Online']/parent::*</value>
      <webElementGuid>f84f4af1-68c3-403f-bc2e-7f451b6a55a8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span</value>
      <webElementGuid>e2113e04-4596-4499-b881-c79d9471b516</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Belajar Bisnis untuk Menjadi Pengusaha Online' or . = 'Belajar Bisnis untuk Menjadi Pengusaha Online')]</value>
      <webElementGuid>244e1878-ceee-4840-895c-097ec53e4860</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
