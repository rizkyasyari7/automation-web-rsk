<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Menjadi Pengusaha Jamu Modern dengan Mempelajari  Teknik Pengolahannya</name>
   <tag></tag>
   <elementGuidId>5b25fc25-535e-4855-9816-d4a854243101</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/div/div[2]/div[2]/div/div/div/div/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.training-title > span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>13ea231c-a6fb-4a95-854f-332049e7ef7f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Menjadi Pengusaha Jamu Modern dengan Mempelajari  Teknik Pengolahannya</value>
      <webElementGuid>cd01468e-00d0-4db5-80bb-1d9a7384c0cb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[1]/div[@class=&quot;header-body-footer-wrapper&quot;]/div[@class=&quot;training body&quot;]/div[@class=&quot;section-1-background&quot;]/div[@class=&quot;container&quot;]/div[@class=&quot;training-short-detail-section&quot;]/div[@class=&quot;detail-section-right&quot;]/div[@class=&quot;training-title&quot;]/span[1]</value>
      <webElementGuid>a45c35db-b570-4ca4-8469-ce504aa1841b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div/div[2]/div[2]/div/div/div/div/span</value>
      <webElementGuid>9c29061a-fdcc-467a-abb7-0ccf0341b05e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Masuk Kelas'])[1]/following::span[1]</value>
      <webElementGuid>79e0bd4e-caf9-4259-9504-c758ded0d1f5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Jadwal Kelas:'])[1]/following::span[1]</value>
      <webElementGuid>3f09c62c-e34b-4d92-b6fa-8066f2990f9a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kartu Prakerja'])[2]/preceding::span[1]</value>
      <webElementGuid>bd01b179-5eef-4dbb-b023-8139c4b48fa6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Manajemen Bisnis'])[1]/preceding::span[1]</value>
      <webElementGuid>db0fe46d-9a1c-4fbf-82d0-57b520f9fde6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div/div/div/div/span</value>
      <webElementGuid>b59ac203-dc7d-42fb-a286-9ed93db0abd1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = 'Menjadi Pengusaha Jamu Modern dengan Mempelajari  Teknik Pengolahannya' or . = 'Menjadi Pengusaha Jamu Modern dengan Mempelajari  Teknik Pengolahannya')]</value>
      <webElementGuid>06a6f9b4-38b2-42b0-9d24-2ec887986833</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
